%global __provides_exclude_from ^%{_libdir}/evince/
%global __requires_exclude ^(%%(find %{buildroot}%{_libdir}/evince/ -name '*.so' | xargs -n1 basename | sort -u | paste -s -d '|' -))

%global libarchive_version 3.6.0
%global poppler_version 22.02.0

Name:           evince
Version:        44.3
Release:        1
Summary:        Document viewer for multiple document formats
License:        GPLv2+ and GPLv3+ and LGPLv2+ and MIT and Afmparse
URL:            https://wiki.gnome.org/Apps/Evince
Source0:        https://download.gnome.org/sources/%{name}/44/%{name}-%{version}.tar.xz

Patch0:         evince-42-poppler-requirement-decrease.patch

BuildRequires:  meson gcc gcc-c++ gettext-devel libtiff-devel gi-docgen 
BuildRequires:  yelp-tools desktop-file-utils libappstream-glib-devel
BuildRequires:  texlive-lib-devel djvulibre-devel
BuildRequires:  pkgconfig(adwaita-icon-theme)
BuildRequires:  pkgconfig(dbus-1)
BuildRequires:  pkgconfig(gio-unix-2.0)
BuildRequires:  pkgconfig(gnome-desktop-3.0) >= 43
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gspell-1)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(gstreamer-base-1.0)
BuildRequires:  pkgconfig(gstreamer-video-1.0)
BuildRequires:  pkgconfig(gtk+-x11-3.0)
BuildRequires:  pkgconfig(libhandy-1)
BuildRequires:  pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libspectre)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(synctex)
BuildRequires:  pkgconfig(libgxps)
BuildRequires:  pkgconfig(libnautilus-extension-4)
BuildRequires:  pkgconfig(poppler-glib) >= %{poppler_version}
BuildRequires:  pkgconfig(libarchive) >= %{libarchive_version}

Provides:       evince-libs evince-dvi evince-nautilus
Requires:       texlive-collection-fontsrecommended
Requires:       nautilus%{?_isa}

%description
Evince is a document viewer for multiple document formats. The goal of evince is to replace the
multiple document viewers that exist on the GNOME Desktop with a single simple application.
Evince is specifically designed to support the file following formats:
PDF, Postscript, djvu, tiff, dvi, XPS, SyncTex support with gedit, comics books (cbr,cbz,cb7 and cbt).

%package        devel
Summary:        Support for developing backends for the evince document viewer
Requires:       %{name} = %{version}-%{release}
Requires:       libarchive%{?_isa} >= %{libarchive_version}
Requires:       poppler-glib%{?_isa} >= %{poppler_version}
Recommends:     gi-docgen-fonts

%description devel
This package contains libraries and header files needed for evince
backend development.

%package_help

%prep
%autosetup -n %{name}-%{version} -p1

%build
export CFLAGS='-I%{_builddir}/%{name}-%{version}/cut-n-paste/synctex %optflags'
%meson -Dcomics=enabled -Ddvi=enabled -Ddjvu=enabled -Dxps=enabled \
       -Dsystemduserunitdir=no -Dnautilus=false \
       -Dps=enabled

%meson_build

%install
%meson_install
%find_lang evince --with-gnome

%delete_la_and_a
install -d $RPM_BUILD_ROOT%{_datadir}/applications

rm -f $RPM_BUILD_ROOT%{_datadir}/metainfo/evince-pdfdocument.metainfo.xml
rm -f $RPM_BUILD_ROOT%{_datadir}/metainfo/evince-psdocument.metainfo.xml
rm -f $RPM_BUILD_ROOT%{_datadir}/metainfo/evince-tiffdocument.metainfo.xml
rm -f $RPM_BUILD_ROOT%{_datadir}/metainfo/evince-comicsdocument.metainfo.xml
rm -f $RPM_BUILD_ROOT%{_datadir}/metainfo/evince-xpsdocument.metainfo.xml

%check
appstream-util validate-relax --nonet $RPM_BUILD_ROOT%{_datadir}/metainfo/org.gnome.Evince.appdata.xml
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Evince.desktop
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Evince-previewer.desktop

%post   -p /sbin/ldconfig
%postun -p /sbin/ldconfig

%files -f evince.lang
%license COPYING
%{_bindir}/*
%{_datadir}/%{name}/
%{_datadir}/applications/*.desktop
%{_datadir}/icons/hicolor/*/apps/org.gnome.Evince*
%{_datadir}/dbus-1/services/org.gnome.evince.Daemon.service
%{_datadir}/glib-2.0/schemas/org.gnome.Evince.gschema.xml
%{_datadir}/GConf/gsettings/evince.convert
%{_datadir}/metainfo/org.gnome.Evince.appdata.xml
%{_datadir}/thumbnailers/evince.thumbnailer
%{_libexecdir}/evinced
%{_libdir}/*.so.*
%{_libdir}/evince/4/backends/*.so
%{_libdir}/evince/4/backends/libdvidocument.so*
%{_libdir}/evince/4/backends/*.evince-backend
%{_libdir}/girepository-1.0/*.typelib
%{_datadir}/metainfo/*.metainfo.xml

%files devel
%{_datadir}/doc/libevview
%{_datadir}/doc/libevdocument
%dir %{_includedir}/evince
%{_includedir}/evince/3.0
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc
%{_datadir}/gir-1.0/*.gir

%files help
%doc NEWS AUTHORS
%{_mandir}/man1/*.1*

%changelog
* Thu Nov 23 2023 zhangxianting <zhangxianting@uniontech.com> - 44.3-1
- update to version 44.3

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn> - 43.1-1
- Update to 43.1

* Mon Jun 20 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.3-2
- fix build when Meson >= 0.61.5
- remove meson option t1lib

* Mon Mar 28 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 42.3-1
- Update to 42.3

* Tue Jun 15 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.2-1
- Upgrade to 3.38.2
- Delete 0001-Resolves-deb-762530-rhbz-1061177-add-man-pages.patch that
  existed in 3.38.2 version
- Use meson rebuild, change 'SYNCTEX_CFLAGS' to 'CFLAGS'

* Mon Jun 8 2020 yanan li <liyanan032@huawei.com> - 3.30.1-4
- Disable designated LIBTOOL directory in %make_build

* Mon Dec 2 2019 chenzhenyu <chenzhenyu13@huawei.com> - 3.30.1-3
- Package init
